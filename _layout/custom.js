
	var listObj = '';

	$(function(){

		if (typeof($.browser.mozilla) != 'undefined' ){
		}

		handle_portfolio();
		handle_navigation();
		handle_contact_form();

	});


	function handle_portfolio(){

		var options = {
		  valueNames: [ 'category' ],
		  page: 8,
		  plugins: [
			  [ 'paging' ]
		  ]
		};

		listObj = new List('work-portfolio-wrapper', options);

		$('.work-nav a').click(function(){
			if ($(this).hasClass('active')) {
				return false;
			}

			var filterItem = this;

			$('.work-nav .active').removeClass('active');
			$(this).addClass('active');


			if ($(this).hasClass('all')){
				listObj.filter();
			}else{
				listObj.filter(function(item) {
					if (item.values().category == $(filterItem).html()) {
						return true;
					} else {
						return false;
					}
				});
			}

			if ($('#work-portfolio-wrapper .paging li').size()>1){
				$('#work-portfolio-wrapper .paging').fadeIn(100);
			}else{
				$('#work-portfolio-wrapper .paging').fadeOut(100);
			}

            return false;
		});

		$('#work-portfolio-wrapper .paging').click(function(){
			$('#work-portfolio a').colorbox();
		});


		//## Portfolio large preview using Colorbox
		//##
		$('#work-portfolio a').colorbox();


		//## Portfolio hover effect
		//##
		$("#work-portfolio li").hover(function(){
			$('div', this).fadeIn(150);
		}, function(){
			$('div', this).fadeOut(150);
		});
	}


	function handle_navigation() {

		$('#navigation a').click(function(){

			var relatedID = $(this).attr('rel');
			var nextPage = this;

			if ($('#'+relatedID).hasClass('active') == false){

				var lastPageID = '#'+$('#pages .active').attr('id');
				var lastPageLayout = '';

				if ($(lastPageID).hasClass('size-small')){ lastPageLayout = 'size-small'; }
				if ($(lastPageID).hasClass('size-large')){ lastPageLayout = 'size-large'; }


				var nextPageID = '#'+relatedID;
				var nextPageLayout = '';

				if ($(nextPageID).hasClass('size-small')){ nextPageLayout = 'size-small'; }
				if ($(nextPageID).hasClass('size-large')){ nextPageLayout = 'size-large'; }

				//console.log('Page: '+lastPageID+' Layout:'+nextPageLayout+' ---- Next Page:'+nextPageID+' Layout:'+nextPageLayout);

				$('#pages > div:visible').fadeOut('fast', function(){
					$(this).removeClass('active').attr('style','');

					if (lastPageLayout != nextPageLayout){
						if (nextPageLayout == 'size-small'){
							$('#layout').animate({ width:'420px',height:'570px', left:'0'}, function(){
								$('#pages > #'+relatedID).fadeIn('fast', function(){
									$(this).addClass('active').attr('style','');
								});
							});
						}

						if (nextPageLayout == 'size-large'){
							$('#layout').animate({ width:'960px',height:'680px', left:'-270px'}, function(){
								$('#pages > #'+relatedID).fadeIn('fast', function(){
									$(this).addClass('active').attr('style','');
								});
							});
						}
					}else{
						$('#pages > #'+relatedID).fadeIn('fast', function(){
							$(this).addClass('active').attr('style','');
						});
					}

					$('#navigation .active').removeClass('active');
					$(nextPage).addClass('active');
				});
			}

			return false;
		});

	}

		function handle_contact_form(){

		//##
		//## Set default values on fields
		//##

		setDefaultField('#frm-name', 'Your name:' );
		setDefaultField('#frm-email', 'Your email:' );
		setDefaultField('#frm-message', 'Your message:' );


		//##
		//## Send message via contact form
		//##
		$('.contact-submit').click(function(){

			$('#frm-name, #frm-email, #frm-message').click().blur();

			//## Form validation (comment this code if you want to remove the validation)
			if ($('.no-value').size()){
				$('.no-value').each(function(){
					$(this).addClass('error');
				});

				$('.frm-state').html('Please complete all the required fields!').addClass('error').fadeIn();

				return false;
			}else{
				$('.frm-state').html('');
			}
			//## End form validation


			var frmName = $('#frm-name').attr('value');						//Get name field value
			var frmMail = $('#frm-email').attr('value');					//Get e-mail field value
			var frmMessage = $('#frm-message').attr('value');				//Get textarea message

			//##
			//## Send data using ajax
			//##
			$.post("mail.php", {action: "sendMail", name: frmName , mail: frmMail, message: frmMessage},			
			function(data){
				if (data.success == '1'){
					//If the mail was sent show the "success" message
					$('.frm-state').html(data.message).removeClass('error').fadeIn();

					$('.frm-name').val('').blur();
					$('.frm-mail').val('').blur();
					$('.frm-message').val('').blur();

				}else{
					//If the mail has failed show the error message
					$('.frm-state').html(data.message).addClass('error').fadeIn();
				}
			},"json");

			return false;
		});

	}


	function setDefaultField(selector, default_value){

		$(selector).click(function(){
			var selector_def_val = default_value;

			if ($(this).val() == ''){
				$(this).addClass('no-value');
			}else{
				if ($(this).val() == selector_def_val) {
					$(this).val('');
					$(this).addClass('no-value');
				}else{
					$(this).removeClass('no-value').removeClass('error');
				}
			}
		});

		$(selector).blur(function(){
			var selector_def_val = default_value;

			if ($(this).val() == ''){
				$(this).val(default_value);

				$(this).addClass('no-value');
			}else{
				if ($(this).val() == selector_def_val) {
					console.log('same shit!');
				}else{
					$(this).removeClass('no-value').removeClass('error');
				}
			}
		});
	}


// Swap content at interval
$(document).ready(function() {

	(function(){
		//$("#page-home").html( "<IMG SRC='http://www.boticus.com/blank/" + Math.round(Math.random() * 9999999 ) + "'>");
		$("#image").attr("src","/blank/" + Math.round(Math.random() * 9999999 ));
		//setTimeout(arguments.callee, 9000);
	})();

});
