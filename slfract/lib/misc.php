<?php
	//Misc functions for SLFract

	function color_format($color) {
		return number_format($color,5);
	}

	function rand_color() {
		$a = color_format(rand(0,100) / 100);
		$b = color_format(rand(0,100) / 100);
		$c = color_format(rand(0,100) / 100);
		return "{$a}*{$b}*{$c}";
	}

	function color_hex2num($hex) {
		return color_format($hex / 0xff);
	}



?>
