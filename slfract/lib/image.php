<?php
	class slfract_image extends slfract_base {
		var $_data;
		var $return;

		function load_jpg($file,$scale=1.0) {

			$im = imagecreatefromjpeg($file);

			$imgSize = GetImageSize($file);
			$imgWidth = $imgSize[0];
			$imgHeight = $imgSize[1];
			$z = 0;
			for ($x=0;$x<$imgWidth;$x++) {
				for ($y=0;$y<$imgHeight;$y++) {
					$rgb = imagecolorat($im, $y, $x);
					$rh = ($rgb >> 16) & 0xFF;
					$gh = ($rgb >> 8) & 0xFF;
					$bh = ($rgb) & 0xFF;

					$rn = color_hex2num($rh);
					$gn = color_hex2num($gh);
					$bn = color_hex2num($bh);

					$fx = ($x * $scale);
					$fy = ($y * $scale);
					$fz = ($z * $scale);

					$this->return[] = "<{$fx},{$fy},{$fz}>*<{$rn},{$gn},{$bn}>";
				}
			}
		}

		function run($step=0,$lpp=20) {
			$total = (count($this->return) / $lpp) - 1;
			$count = "0";
			$out = array();
			foreach($this->return as $item) {
				$count++;
				if ((($step * $lpp) < $count) and (($step + 1) * $lpp) >= $count) {
					$out[] = $item;
				}
			}
			return array($total,$out);
		}
	}
?>