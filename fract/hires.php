<?
	$img = $_REQUEST["i"];
	
	$bgc = 'black';
	if (isset($_REQUEST["bg"])) $bgc = str_replace("_","#",$_REQUEST["bg"]);

	//$image->setBackgroundColor(new ImagickPixel('transparent')); 
	$image = new Imagick('output/'.$img.'.png');
	
	$flattened = new Imagick();

	$flattened->newImage($image->getImageWidth(), $image->getImageHeight(), new ImagickPixel( $bgc ));	

	$flattened->compositeImage($image, imagick::COMPOSITE_OVER,0,0);
	$flattened->setImageFormat("jpg");
	header('Content-type: image/jpeg');
	echo $flattened;
	
?>
