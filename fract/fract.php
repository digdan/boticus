<?php

	class fract {
		var $layers; //Array of layers
		var $flameSource;

		function __construct() {
		}

		public function fractal($flame=NULL,$flameb=NULL,$res=1,$cquality=1,$bgcolor=1) {
			//$res is a factor of 720px. 1 res = 720 pixels square size.
			if ( is_null($flame) ) return false;

			//Define target png
			$temp_path = "/tmp/".uniqid().".png";

			if ( ! is_null($flameb)) {
				//Write flame to tmp file
				$fp = fopen("/www/boticus.com/www/fract/tmp/source-a.flame","w");
				fputs($fp,$flame);
				fclose($fp);

				$fp = fopen("/www/boticus.com/www/fract/tmp/source-b.flame","w");
				fputs($fp,$flameb);
				fclose($fp);

				$command = "env method=union symmetry=3 template=/www/boticus.com/www/fract/flames/vidres.flame cross0=/www/boticus.com/www/fract/tmp/source-a.flame cross1=/www/boticus.com/www/fract/tmp/source-b.flame /usr/bin/flam3-genome > /www/boticus.com/www/fract/tmp/target.flame";

				system( $command );


			} else {
				//Write flame to tmp file
				$fp = fopen("/www/boticus.com/www/fract/tmp/target.flame","w");
				fputs($fp,$flame);
				fclose($fp);
			}


			$this->flameSource = file_get_contents('/www/boticus.com/www/fract/tmp/target.flame');

			$command = "env 'out={$temp_path}' format=png transparency={$bgcolor} ss={$res} qs={$cquality} /usr/bin/flam3-render < /www/boticus.com/www/fract/tmp/target.flame";

			system( $command );

			$temp_obj = new Imagick(); //Load into layer

			$fp = fopen($temp_path,"r");
			$temp_obj->readImageFile($fp);

			fclose($fp);

			$this->layers[] = $temp_obj;

			unlink( $temp_path );
			return true;
		}

		public function image($img) {
			$this->layers[] = new Imagick($img);
		}

		public function save($image,$path) {
			$fp = fopen($path,"wb");
			fputs($fp,$image);
			fclose($fp);
		}

		public function display($return=false,$h=592,$w=720,$type="jpeg") {
			$last_layer = false;

			$master = new Imagick();
			$master->newImage($w,$h, new ImagickPixel('transparent') );
			$master = array_shift($this->layers);

			if (count($this->layers) > 0) {
				foreach($this->layers as $layer) { //Flatten layers
					$master->compositeImage( $layer , Imagick::COMPOSITE_ATOP , 0 , 0 );
				}
				//$master->scaleImage(0,400);
			}

			if ($type=="jpeg") {
        		$master->setImageCompressionQuality(100);
				$master->setImageFormat('JPEG');

				if ( ! $return ) {
					header("Content-type: image/jpeg "); // Out with it
					echo (string)$master;
				} else {
					return (string)$master;
				}
			}

			if ($type=="png") {
				$master->setImageCompression(\Imagick::COMPRESSION_UNDEFINED);
				$master->setImageCompressionQuality(0);
				$master->setImageFormat('PNG');

				if ( ! $return ) {
					header("Content-type: image/png ");
					echo (string)$master;
				} else {
					return (string)$master;
				}
			}

		}

		public function send_image($image=NULL,$flame_array=NULL,$type="jpeg") {
			if ( is_null($image) ) return false;
			$name = uniqid().".".$type;
			if (is_null($flame_array)) {
				$flame_text = "Unknown";
			} else {
				$flame_text = join("/",$flame_array);
			}

			$from_name = "Fractalverse";
			$from_mail = "dan@danmorgan.net";
			$to_name = "Fractalverse";
			//$mailto = "magic243stable@m.facebook.com";
			$mailto = "dan@danmorgan.net";
			$message = "";
			$message .= "HiRes Render : <B>{$flame_text}</B><BR>\n\n";
			$message .= "<A HREF=\"http://www.boticus.com/fract/deploy.php?a={$flame_array[0]}&b={$flame_array[1]}\">Render Hi-res</A><BR>\n\n";
			$message .= "Flame A {$flame_array[0]} : <A HREF=\"http://www.boticus.com/fract/admin.php?flame_id={$flame_array[0]}\"> {$flame_array[0]} </A><BR>\n\n";
			$message .= "Flame B {$flame_array[1]} : <A HREF=\"http://www.boticus.com/fract/admin.php?flame_id={$flame_array[1]}\"> {$flame_array[1]} </A><BR>\n\n";

			$message .="<img src='data:image/jpg;base64,".chunk_split(base64_encode((string)$image),64,"\r\n")."'>";

			$message .= "<fieldset><legend>Flame Source</legend><xmp>\r\n";
			$message .= $this->flameSource."\r\n";
			$message .= "</xmp></fieldset>\r\n";

			$subject = "Fractalverse Pattern {$flame_text}";

			$header = "From: ".$from_name." <".$from_mail.">\r\n";
			$header .= 'Content-type:text/html; charset=ISO-8859-1\r\n';
			
			/*
			$content = chunk_split(base64_encode((string)$image));

			$uid = md5(uniqid(time()));

			$header .= "MIME-Version: 1.0\r\n";
			$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
			$header .= "This is a multi-part message in MIME format.\r\n";
			$header .= "--".$uid."\r\n";
			//$header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
			$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
			$header .= $message."\r\n\r\n";
			$header .= "--".$uid."\r\n";
			$header .= "Content-Type: image/{$type}; name=\"".$name."\r\n";
			$header .= "Content-ID: {$cid}\r\n";
			$header .= "Content-Transfer-Encoding: base64\r\n";
			$header .= "Content-Disposition: attachment; filename=\"".$name."\r\n\r\n";
			$header .= $content."\r\n";
			$header .= "--".$uid."--";
			*/
			if (mail($mailto, $subject, $message, $header)) {
			    echo "Sent.\n";
			} else {
				echo "Error.\n";
			}
		}

	}
?>
