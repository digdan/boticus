<?php
include_once("util.php");
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*
* Class is used to decode and render Words for Boticus
*
*/

	class boticus {
		function __construct() {
			$this->ci =& get_instance();
			$this->ci->load->database();
		}

		function decode($poem=NULL,$seed=NULL) {

			if (! is_null($poem)) {
				if ( ! is_null($seed)) {
					srand($seed);
				}
				return $this->s($this->r($poem));
			}
		}

		function nonseq($amount,$iter=0,$seed=NULL) {

		    if (!$seed) {
		        list($usec, $sec) = explode(' ', microtime());
		        $seed = (float) $sec + ((float) $usec * 100000);
		    }

		    srand(crc32($seed));

		    for($i=0;$i<$amount;$i++) $map[$i] = ($i + 1);

		    for($i=(count($map)-1);$i>=0;$i--) {
		        $j = @rand(0,$i);
		        $tmp = $map[$i];
		        $map[$i] = $map[$j];
		        $map[$j] = $tmp;
		    }

		    if ($iter > ($amount)) {
		        $base = floor($iter / $amount);
		        $niter = $iter - ($base * $amount);
		        $prec = $this->nonseq($amount,($niter + 1),($seed+1));
		        return $prec + ($base * $amount);
		    }
			if ( ! $iter ) return false;
		    return $map[$iter-1];
		}

		function fy(&$items,$seedstring) {
			if (!$seedstring) {
				$seedval = time();
			} else {
				if (is_numeric($seedstring)) {
					$seedval = $seedstring;
				} else {
					$seedval = crc32($seedstring);
				}
			}

			srand($seedval);
			for ($i = count($items) - 1; $i > 0; $i--) {
				$j = @rand(0, $i);
				$tmp = @$items[$i];
				@$items[$i] = $items[$j];
				@$items[$j] = $tmp;
			}
			return $items;
		}

/**
* w - fetches thesaurus record of a word
*
* @param string $word - Word to lookup
* @return array - array of word types
*/

		function w($word) {
			$mword = $this->ci->db->escape($word);
			$tn = time();
			$thres = $tn - 2592000;
			$gquery = "SELECT id,data from word_cache where word = {$mword} and cached > {$thres}";
			$query = $this->ci->db->query($gquery);

			if ($query->num_rows() > 0) {
				$gres = $query->result();
				$data = json_decode($gres[0]->data);
				return $data;
			} else {
				$uword = urlencode($word);
				$mword = $this->ci->db->escape($word);
				$url = "http://words.bighugelabs.com/api/2/d604ce357267208b789840ebc490d772/{$uword}/json";
				$data = @json_decode(@file_get_contents($url));
				if ($data) {
					$nquery = "DELETE from word_cache where word = {$mword}";
					$this->ci->db->query($nquery);
					$mdata = $this->ci->db->escape(json_encode($data));
					$gquery = "INSERT into word_cache (word,data,cached) VALUES ('{$word}',{$mdata},{$tn})";
					$this->ci->db->query($gquery);
				}
				return $data;
			}
		}

		function ws(&$array) { //Weighted Shuffle
			foreach($array as $k=>$v) $tmp_array[$k] = rand(0,$v);
			arsort($tmp_array);
			foreach($tmp_array as $k=>$v) $out_array[$k] = $array[$k];
			$array = $out_array;
		}

		static function ns(&$list) { //Associative Shuffle
			if (!is_array($list)) return $list;
			$keys = array_keys($list);
			shuffle($keys);
			$random = array();
			foreach ($keys as $key)	$random[$key] = $list[$key];
			return $random;
		}

		function r($string) {
			$cp = 0; $tolken = 0; $top = ''; $tolkens = array(); $buffer = '';
		    for($i=0;$i<strlen($string);$i++) {
		        $char = substr($string,$i,1);
		        if ($char == "[") {
		            if ($cp == 0) $tolken++; else $buffer .= "[";
		            $cp++;
		        } elseif ($char == "]") {
		            $cp--;
		            if ($cp > 0) {
		                $buffer .= $char;
		            } else {
		                $top .= " WORD{$tolken} ";
						$bp = explode(":",$buffer);
						if (count($bp) == 1) {
							$tolkens[$tolken]["text"] = $buffer;
							$tolkens[$tolken]["flags"] = array();
						} else {
							$tolkens[$tolken]["flags"] = str_split($bp[1]);
							$tolkens[$tolken]["text"] = $bp[0];
						}
		                $buffer = "";
		            }
		        } else {
		            if ($cp > 0) $buffer .= $char; else $top .= $char;
		        }
		    }

			foreach($tolkens as $tolken=>$tdata) {

				$order_reverse = false;
				$pre_choice = '';
				$include_self = true;
				$ucase = false;
				$order_equal = false;
				$new_only = false;
				$chance_arr = array();
				$out_arr = array();
				$part = "";
				$kind = "";
				$out_limit = 999;

				$new_word_info = $this->w($tdata["text"]); //Fetch information on word

				if (!$new_word_info) return false;
				if (is_array($tdata["flags"])) {

					//Word Parts
					if (in_array("n",$tdata["flags"])) {
						if (isset($new_word_info->noun)) {
							$part = $new_word_info->noun;
						}
					}
					if (in_array("j",$tdata["flags"])) {
						if (isset($new_word_info->adjective)) {
							$part = $new_word_info->adjective;
						}
					}
					if (in_array("a",$tdata["flags"])) {
						if (isset($new_word_info->adverb)) {
							$part = $new_word_info->adverb;
						}
					}
					if (in_array("v",$tdata["flags"])) {
						if (isset($new_word_info->verb)) {
							$part = $new_word_info->verb;
						}
					}

					if ( ! isset($part)) {
						foreach($new_word_info as $part) {
							foreach($part as $kind) {
								foreach($kind as $a_word) {
									$out_arr[] = $a_word;
								}
							}
						}
					}

					if (in_array("s",$tdata["flags"])) { // Sym
						if (isset($part->syn)) {
							$out_arr = $part->syn;
						} else {
							foreach($part as $kind) {
								foreach($kind as $a_word) {
									$out_arr[] = $aword;
								}
							}
						}
					}
					if (in_array("!",$tdata["flags"])) { //Anti
						$include_self = false;
						if (isset($part->ant)) {
							$out_arr = $part->ant;
						} else {
							foreach($part as $kind) {
								foreach($kind as $a_word) {
									$out_arr[] = $aword;
								}
							}
						}
					}
					if (in_array("r",$tdata["flags"])) { // Rel
						if (isset($part->rel)) {
							$out_arr = $part->rel;
							$include_self = false;
						} else {
							foreach($part as $kind) {
								foreach($kind as $a_word) {
									$out_arr[] = $a_word;
								}
							}
						}
					}
					if (in_array("i",$tdata["flags"])) { // Sim
						if (isset($part->sim)) {
							$out_arr = $part->sim;
							$include_self = false;
						} else {
							foreach($part as $kind) {
								foreach($kind as $a_word) {
									$out_arr[] = $aword;
								}
							}
						}
					}
					if (in_array("u",$tdata["flags"])) { // Usr
						if (isset($part->usr)) {
							$out_arr = $part->usr;
							$include_self = false;
						} else {
							foreach($part as $kind) {
								foreach($kind as $a_word) {
									$out_arr[] = $aword;
								}
							}
						}
					}

					//Special Flags
					for($i=1;$i<=9;$i++) {
						if (in_array($i,$tdata["flags"])) { //Only choose between the first X choices
							$out_limit = $i;
						}
					}

					if (in_array("o",$tdata["flags"])) { // Reverse odds (end of list more likley)
						$order_reverse = true;
					}

					if (in_array("l",$tdata["flags"])) { // Use last chosen word matching text
						$pre_choice = $GLOBALS["last"][$tdata["text"]];
					}

					if (in_array("d",$tdata["flags"])) { //Do not include original word
						$include_self = false;
					}

					if (in_array("=",$tdata["flags"])) { //Give all options equal chance
						$order_equal = true;
					}

					if (in_array("m",$tdata["flags"])) { //Force new word
						$new_only = true;
					}

					if (in_array("c",$tdata["flags"])) { //Caps the case
						$ucase = true;
					}
				}

				if (( ! $part ) and ( ! $out_arr )) {
					foreach($new_word_info as $part) {
						foreach($part as $kind) {
							foreach($kind as $a_word) {
								$out_arr[] = $a_word;
							}
						}
					}
				}

				//Build out_array if is none
				if (( $part ) and ( ! $out_arr )) {
					foreach($part as $kind) {
						foreach($kind as $a_word) {
							$out_arr[] = $a_word;
						}
					}
				}

				//Limit selections per out_limit
				if (count($out_arr) > $out_limit) {
					$out_arr = array_slice($out_arr,0,$out_limit);
				}

				//We including the source word?
				if ($include_self) array_unshift($out_arr,$tdata["text"]);

				if ($new_only) { //Removed preselected words as options
					$ochance_arr = $chance_arr;
					$chance_arr = array();
					foreach($ochance_arr as $word=>$weight) {
						if ( ! $GLOBALS["last"][$word] ) {
							$chance_arr[$word] = $weight;
						}
					}
				}

				//Make words at the (start/end) of the list more likely used based on order_reverse
				$count = ($order_reverse ? 0 : count($out_arr)+1);
				foreach($out_arr as $oak=>$oav) {
					$count = ($order_reverse ? $count + 1 : $count - 1);
					$chance_arr[$oav] = $count;
				}

				if (! $order_equal) {
					$this->ws($chance_arr); //Weighted Shuffle
					reset($chance_arr);
				} else {
					$this->ns($chance_arr);
				}
				$choice = ( $pre_choice ? $pre_choice : key($chance_arr));
				$GLOBALS["last"][$tdata["text"]] = $choice;
				if ($ucase) $choice = ucfirst($choice);
				$top = str_replace(" WORD{$tolken} ",$choice,$top);

			}
			return $top;
		}

		function s($string,$depth=0) {
			if ( ! @$GLOBALS["map"] ) $GLOBALS["map"] = array();
		    $cp = 0; $tolken = 0; $top = ''; $tolkens = array(); $buffer = '';
		    for($i=0;$i<strlen($string);$i++) {
		        $char = substr($string,$i,1);
		        if ($char == "{") {
		            if ($cp == 0) $tolken++; else $buffer .= "{";
		            $cp++;
		        } elseif ($char == "}") {
		            $cp--;
		            if ($cp > 0) {
		                $buffer .= $char;
		            } else {
		                $top .= " TOLKEN{$tolken} ";
		                $tolkens[$tolken]["text"] = $buffer;
		                $buffer = "";
		            }
		        } else {
		            if ($cp > 0) $buffer .= $char; else $top .= $char;
		        }
		    }

		    if (is_array($tolkens)) {
				foreach($tolkens as $k=>$v) {
					$tolkens[$k]["data"] = $this->s($v["text"],$depth+1);
				}
			}



			$inv = explode(":",$top);

			if (count($inv) > 1) {
				$index = $inv[0];
				$top = $inv[1];
				$forced = $GLOBALS["map"][$index];
			} else {
				$top = $inv[0];
				$forced = NULL;
			}

			$dice = explode("|",$top);
			$count="0";
			foreach($dice as $k=>$v) {
				$choices[] = $count++;
			}
			shuffle($choices);

			$choice = $choices[0]; //Choose first key after shuffle for random


			if ( ! is_null(@$forced) ) {
				$choice = $forced; //Enforce
			}

			$GLOBALS["map"][] = $choice; //Add current to list

			$newtext = $dice[$choice];



		    if (is_array($tolkens)) {
				foreach($tolkens as $k=>$v) {
					$newtext = str_replace(" TOLKEN{$k} ",$tolkens[$k]["data"]["text"],$newtext);
				}
			}

		    if ($depth==0) {
				return $newtext;
			} else { //Recurse
				 return array("text"=>$newtext,"tolkens"=>$tolkens,"dice"=>$dice[0],"depth"=>$depth);
			}
		}


		function shuffle_assoc($list) {
			if (!is_array($list)) return $list;

			$keys = array_keys($list);
			shuffle($keys);
			$random = array();
			foreach ($keys as $key) {
				$random[] = $list[$key];
			}
			return $random;
		}

/**
* numeral2img - Converts roman numerals to roman images
*
* @param string $numerals - String to convert
* @return $string HTML string of class defs
*/
		function numeral2img($numerals) {
			$out = "";
			for ($i=0;$i<strlen($numerals);$i++) {
				$char = $numerals[$i];
				$out .= "<img src=\"/_media/".strtolower($char).".png\">";
			}
			return $out;
		}

/**
* content - Front page content renderer
*
* @param int $forced - Forces to use a specific pattern
* @return $string Boticus Quote
*/
		function content($forced=NULL,$seed=NULL) {
			//$seed = date("gz"); //Day of the Year + 24 Hour of the day to generate seeds
			$query = $this->ci->db->query("SELECT count(id) as cnt from patterns");
			$ra = $query->row_array();
			$max = $ra["cnt"]-1;

			if ($mark = $this->ci->session->userdata('mark')) {
				if ($mark > $max) {
					$mark = 0;
				} else {
					$mark++;
				}
				$this->ci->session->set_userdata('mark',$mark);
			} else {
				$mark = 1;
				$this->ci->session->set_userdata('mark',$mark);
			}

			if ( is_null($seed)) {
				$seed = rand();
				srand($seed);
			} else {
				srand(crc32($seed));
			}

			srand($seed);

			if (is_null($forced) ) {
				$ad = $this->ci->session->all_userdata();
				//$offset = $this->nonseq($max,$mark,$ad["session_id"]);
				$offset = round(rand(0,$max));
				$query = $this->ci->db->query("SELECT * from patterns limit {$offset},1");
			} else {
				$forced_clean = $this->ci->db->escape($forced);
				$query = $this->ci->db->query("SELECT * from patterns where id = {$forced_clean}");
			}
			$pattern_record = $query->row_array();
			$pattern = strip_tags($pattern_record["pattern"],"<B><I><U><LI><DIV>");

			$query = $this->ci->db->query("UPDATE patterns set used = ( used + 1 ) where id = {$pattern_record["id"]}");
			$numerals = util::roman_numeral($pattern_record["id"]);

			return array("content"=>$this->decode($pattern,$seed),"numeral"=>$this->numeral2img($numerals));
		}

/**
* Image - Generates an image of boticus content for memes
*
*/
		function image($forced=NULL,$seed=NULL) {
			$content = $this->content($forced,$seed);
			$map = array(
				"book"=>array(
 					"bgcolor"=>"#FFFFFF",
					"alpha"=>"0.0",
					"textcolor"=>"#50483a",
					"image"=>"stage_bg_book.png",
				),
				"scroll"=>array(
					"bgcolor"=>"#FFFFFF",
					"alpha"=>"0.0",
					"textcolor"=>"#50483a",
					"image"=>"stage_bg_scroll.png",
				),
				"cosmos"=>array(
					"bgcolor"=>"#543155",
					"alpha"=>"0.28",
					"textcolor"=>"#FFFFFF",
					"image"=>"stage_bg_cosmos.png",
				),
				"ancient-spotlight"=>array(
					"bgcolor"=>"#FFFFFF",
					"alpha"=>"0.0",
					"textcolor"=>"#FFFFFF",
					"image"=>"stage_bg_ancient-spotlight.png",
				),
				"columns"=>array(
					"bgcolor"=>"#251e0e",
					"alpha"=>"0.74",
					"textcolor"=>"#FFFFFF",
					"image"=>"stage_bg_columns.png",
				),
			);

			$dice=array();
			foreach($map as $map_name=>$values) {
				$dice[] = $map_name;
			}
			$dice = $this->fy($dice,$seed);
			$conf = $map[$dice[0]];


			//$conf = array_shift($map); //Stage configuration
			$bg_path = "_content/generator/{$conf["image"]}";
			$boticuses = glob("_content/generator/stage_boticus_*.png");
			shuffle($boticuses);
			//$border_path = "/www/hosts/boticus.com/www/_content/generator/stage_boticus_desaturated-1-flip.png";
			$boticus_path = $boticuses[0];
			$str = strip_tags(html_entity_decode($content["content"]));

			$w = 420;
			$h = 520;
			$font = "_content/fonts/trajanprobol.ttf";
			// Inversly size font to length of string. Multiply by scale.
			$font_size = sqrt(1 / strlen($str)) * 250;

			$padding = 220; // Side padding
			$angle = 0;
			$y_padding = 120; //Where to start from y

			$bg= new Imagick();
			$bg->newImage($w, $h, $conf["bgcolor"], "jpg");

			$img1 = new Imagick( $bg_path );
			$img2 = new Imagick( $boticus_path );

			$bg->compositeImage( $img1, imagick::COMPOSITE_DEFAULT, 0, 0 );
			$bg->compositeImage( $img2, imagick::COMPOSITE_DEFAULT, 0, 0 );


			$draw = new ImagickDraw();

			$draw->setFontSize($font_size);
			if (@$font) {
				$draw->setFont($font);
			}
			$draw->setTextAlignment(2);
			$color = new ImagickPixel($conf["textcolor"]);
			$draw->setFillColor($color);


			$ww = (floor(($w - ($padding / 2)) / $font_size) ) * 2;
			// (width minus half the padding divided by the font size ) times two
			$str = wordwrap($str, $ww ,"\n");
			$str_array = explode("\n",$str);
			$str_array[] = str_repeat(" ",$ww * 2)."~ Boticus";

			$x = $w / 2;
			//$y = ($mp - (count($str_array) * $font_size))+$y_padding; //Base Center
			$y = $y_padding;

			foreach($str_array as $line_num=>$line) {
				if ($line_num == (count($str_array) - 1)) {
					$draw->setFontSize(round($font_size / 2));
					$y = $y - 12;
				}
				$bg->annotateImage( $draw , $x, $y + ($line_num * ($font_size)), $angle, $line );
			}

			header('Content-type: image/png');
			$bg->setImageFormat('png');
        		$bg->setImageCompressionQuality(90);
			echo $bg;

		}

	}
?>
