<html prefix="og: http://ogp.me/ns#">
<head>
<title>{$content}</title>
<meta property="og:title" content="{$content} ~ Boticus" />
<meta property="og:url" content="{$content} ~ Boticus" />
<meta property="og:type" content="image" />
<meta property="og:image" content="http://www.boticus.com/blank/{$seed}" />
<meta property="og:image:type" content="image/png" />
<meta property="og:image:url" content="http://www.boticus.com/blank/{$seed}" />
<meta property="og:image:width" content="420" />
<meta property="og:image:height" content="520" />
</head>
</html>