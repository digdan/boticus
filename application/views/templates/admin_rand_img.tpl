<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Ancient Databanks of Boticus</title>

<link rel="stylesheet" href="/_layout/reset.css" />
<link rel="stylesheet" href="/_layout/text.css" />
<link rel="stylesheet" href="/_layout/960.css" />
<link rel="stylesheet" href="/_layout/admin.css" />

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script>
<script type="text/javascript" src="/_layout/admin.custom.js"></script>
<script type="text/javascript" src="/_layout/scripts/js/jquery-ui-1.8.21.custom.min.js"></script>


<script type="text/javascript" src="/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
        // General options
        mode : "textareas",

		height: "400",
        theme : "advanced",
        plugins : "autolink,spellchecker,style,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,media,searchreplace,contextmenu,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,templates",

        // Theme options
        theme_advanced_buttons1 : "templates,bold,italic,underline,strikethrough,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "link,unlink,anchor,image,cleanup,code,insertdate,inserttime,preview,forecolor,backcolor,hr,removeformat,sub,sup,charmap,emotions,iespell,spellchecker,attribs,undo,redo",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : false,
        theme_advanced_resizing : true,
});

	$(function() {
		$( "#accordion" ).accordion({literal}{ collapsible: true {/literal}{$switch_accordian}{literal}}{/literal});
		$('#view_form').submit(function() {
			$('#view_content').val($('#content').val());
			window.open('', 'formpopup', 'width=400,height=400,resizeable,scrollbars');
			this.target = 'formpopup';
		});

	});
	</script>
</script>



</head>
<body>
	<h1>
		~ Ancient Databanks of Boticus ~
	</h1>
	<div class="container_24" style="border:1px solid #ADADAD;">
		<HR>
		<div class="grid_4">

		</div>
		<div class="grid_14">
			<BR><BR>
			<CENTER>
			<A class="lookbutton" HREF="/admin/rand_img">REFRESH</A>
			<BR>
			<IMG SRC="http://www.boticus.com/blank/{$seed}">
			<BR>
			<A TARGET="_BLANK" class="lookbutton" HREF="/admin/send_image/{$seed}">Send Image</A>
			</CENTER>
		</div>
		<div class="grid_6">
			<UL>
				<LI><A HREF="/admin/rand_img">Facebook IMG<A></LI>
				<LI><A HREF="/admin">New Pattern</A></LI>
			</UL>
			<div id="accordion">
			<h3><a href="#">Key</a></h3>
				<div>

					<B>Randomizer : </B><BR>
					<I>{literal}{choice one|choice two|choice three}{/literal}</I> <BR>
					You may follow chosen positions :<BR>
					<I>{literal}{X:You chose one|You chose two|You chose three}{/literal}</I> <BR>
					where X is the position # the tolken was processed.<BR>
					<BR><BR>
					<B>Thesauruser : </B><BR>
					[word] or [word:flags]
					<BR><BR>

					<B>Thesauruser Flags : </B><BR>
					<ul>
						<li> n - Noun, use a person place or thing sense of the word</li>
						<li> j - Adjetive, use a descriptive sense of the word</li>
						<li> a - Adverb, use a descriptive action sense of the word</li>
						<li> v - Verb, use an action sense of the word</li>
					</ul>
					<ul>
						<li> s - Use a synonymous word (default)</li>
						<li> ! - Use an antonymous word</li>
						<li> r - Use a relative word</li>
						<li> i - Use a similar word</li>
						<li> u - Use a usr word</li>
					</ul>
					<ul>
						<li> o - Reverse order of possible words</li>
						<li> l - Use last matching word</li>
						<li> d - Do not include original word</li>
						<li> = - Give all words in list equal chance of selection</li>
						<li> m - Force a new word (opposite of "l")</li>
						<li> c - Capitalize first letter of chosen word</li>
					</ul>
					</p>
				</div>

				<h3><a href="#">Saved Patterns</a></h3>
				<div style="overflow:scroll;">
					<table>

						{foreach from=$patterns item=pattern}
							<TR id="pattern_{$pattern.id}">
							<TD>{$pattern.id}.</TD>
							<TD><A HREF="/admin/{$pattern.id}">{$pattern.name}</A></TD>
							<TD><A href="#" onClick="$('#pattern_{$pattern.id}').load('/ajax/pattern_delete/{$pattern.id}');"><IMG SRC="/_media/delete_small.png"></A></TD>
							</TR>
						{/foreach}
					</table>
				</div>
			</div>
		</div>
	</div>

<div class="clear"></div>

</body>
</html>