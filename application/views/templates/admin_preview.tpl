<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<head>
<style>
.admin_preview {
	width:100%;
}
.admin_preview p {
	word-wrap:break-word;
}
</style>
</head>
<body>
	<FORM METHOD="POST" ACTION="/admin/view">
	<INPUT TYPE="hidden" name="content" value="{$pre_content|htmlspecialchars}">
	<INPUT TYPE="submit" name="cmd" value="Again">
	</FORM>
	<HR>
	<DIV class="admin_preview">
		<p>{$content}</p>
	</DIV>
	<HR>
	<FORM METHOD="POST" ACTION="/admin/send_tweet">
		<INPUT TYPE="hidden" name="tweet_content" value="{$content|htmlspecialchars}">
		<INPUT TYPE="submit" name="cmd" value="Tweet">
	</FORM>

</body>
</html>