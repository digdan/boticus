<?php /* Smarty version Smarty-3.1.11, created on 2017-09-06 17:07:00
         compiled from "/var/www/app/application/views/templates/front_page.tpl" */ ?>
<?php /*%%SmartyHeaderCode:201398351759ae2478cbc4b2-69277251%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd8b3eb3554557ecc914de074ea75367228c892e2' => 
    array (
      0 => '/var/www/app/application/views/templates/front_page.tpl',
      1 => 1504717609,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '201398351759ae2478cbc4b2-69277251',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.11',
  'unifunc' => 'content_59ae2478cbe596_73468317',
  'variables' => 
  array (
    'meta' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59ae2478cbe596_73468317')) {function content_59ae2478cbe596_73468317($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
    "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html dir="ltr" lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<!--
__________        __  .__
\______   \ _____/  |_|__| ____  __ __  ______
 |    |  _//  _ \   __\  |/ ___\|  |  \/  ___/
 |    |   (  <_> )  | |  \  \___|  |  /\___ \
 |______  /\____/|__| |__|\___  >____//____  >
        \/                    \/           \/
-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

		<title><?php echo $_smarty_tpl->tpl_vars['meta']->value['title'];?>
</title>

		<!-- Styles -->
		<link rel='stylesheet' href='_layout/style.css' type='text/css' media='all' />
		<link rel='stylesheet' href='_layout/scripts/jquery.colorbox/colorbox.css' type='text/css' />

		<!-- Fonts -->

		<link href='http://fonts.googleapis.com/css?family=Signika:400,600,700,300' rel='stylesheet' type='text/css' />

		<!-- Scripts -->
		<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js?ver=1.7'></script>
		<script type="text/javascript" src="/_layout/scripts/js/jquery-ui-1.8.21.custom.min.js"></script>

		<script type='text/javascript' src='_layout/scripts/jquery.cycle.lite.js'></script>
		<script type='text/javascript' src='_layout/scripts/list.min.js'></script>
		<script type='text/javascript' src='_layout/scripts/list.paging.min.js'></script>
		<script type='text/javascript' src='_layout/scripts/jquery.jrumble.js'></script>
		<script type='text/javascript' src='_layout/scripts/jquery.timer.js'></script>
		<script type="text/javascript" src="_layout/scripts/jquery.colorbox/jquery.colorbox-min.js"></script>
		<script type='text/javascript' src='_layout/custom.js'></script>
		<script type="text/javascript">
			// Big brother still keeps tabs on him
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-777209-21']);
			_gaq.push(['_trackPageview']);

			(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
	</head>
	<body>

		<div id="layout-wrapper">
			<div id="layout">
				<div id="pages">
					<div id="page-home" class="active size-small">
						<IMG ID="image">
					</div>

					<div id="page-story" class="size-large">
						<div class="work-wrapper">
							<div id="top-section" class="clearfix">
							</div>

							<div class="left">
								<div class="front-cell">
								<IMG src="/_media/slide-1-sized.jpg" class="front-slide">
								<h2 class="strong-title">Discovery</h2>
								<p>While breaking ground in the woody outskirts just north east of Athens, a digging worker made a discovery of an ancient crude statue.</p> <p>Teams of archaeologist worked tirelessly to carefully excavated the bronzed mannequin.</p>
								<BR><BR>
								<p><audio controls="controls" style="width:190px"><source src="/_media/bg.ogg" type="audio/ogg" /></audio></p>
								</div>
								<span class="front-divide"></span>
								<div class="front-cell">
								<IMG src="/_media/slide-2-sized.jpg" class="front-slide">
								<h2 class="strong-title">Awakening</h2>
								<p>The discovery from Greek antiquity was accidentally activated while a crew of research scientists moved the body.</p><p>Government intervened and declared that the ancient bronze android was harmless, then gifted the metal man back to scientific communities.</p>
								</div>
								<span class="front-divide"></span>
								<div class="front-cell">
								<IMG src="/_media/slide-3-sized.jpg" class="front-slide">
								<h2 class="strong-title">Damage</h2>
								<p>The soulless sentient proclaimed his name was Boticus and told detailed stories of the fall of ancient empires due to emerging religious beliefs and ancient discoveries of evolution.</p>
								</div>
								<span class="front-divide"></span>
								<div class="front-cell">
								<IMG src="/_media/slide-4-sized.jpg" class="front-slide">
								<h2 class="strong-title">Future</h2>
								<p>Now he spends his time questioning the future, laying around, playing his bronze lyre, all while constantly talking and spilling his wine.</p><p>Sometimes he gets on my nerves.</p>
								</div>
							</div>

						</div>
					</div>

					<div id="page-donate" class="size-small">
						<div class="work-wrapper">
							<div id="top-section" class="clearfix">
								<div class="left">
									<h1 class="user-name work"><span>Help</span> Boticus</h1>
									<h3 class="user-subtitle work">He is made of bronze, not gold</h3>
								</div>
							</div>

							<div class="left">
								<h2 class="strong-title">Donate</h2>
								<p>There has been a great deal of work put into Boticus. While his existence may not be pragmatic, he is an oddity to experience. Help out our team so that Boticus can avoid the junkyard and flourish.</p>
							</div>

							<div style="padding-left:110px;">
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="CVKDRPCWQYGZW">
<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
							</div>
<img src="/_media/sadbot.jpg" width="346">
								<ul class="paging"></ul>
						</div>
					</div>



					<div id="page-contact" class="size-small">
						<div class="wrapper-contact">
							<div id="top-section" class="clearfix">
								<div class="left">
									<h1 class="user-name contact">Bot<span>icus</span></h1>
									<h3 class="user-subtitle contact">Contact the team</h3>
								</div>
							</div>

							<div class="left">
								<p>Want to contact the scientists caring for Boticus? Maybe you want to talk about your feelings instead? Have a contact form : </p>
							</div>
							<form method="post" action="/contact.php">
							<div id="contact-form">
								<input id="frm-name" type="text" value="" placeholder='Name'/>
								<input id="frm-email" type="text" value="" placeholder='Email' />
								<textarea id="frm-message" rows="" cols="" placeholder='Your Message'></textarea>
								<input type='submit' class="button contact contact-submit" name="cmd" value='Send'>

								<div class="frm-state-wrapper">
									<span class="frm-state"></span>
								</div>
							</div>
							</form>

						</div>
					</div>

					<!-- #### Contact Page - End -->

				</div>

				<!-- #### Navigation Section -->

				<div id="navigation">
					<ul>
						<li><a href="#" rel="page-home" class="active">Boticus</a></li>
						<li><a href="#" rel="page-story">The Story</a></li>
						<li><a href="#" rel="page-contact">Contact</a></li>
					</ul>
				</div>


				<!-- #### Navigation Section - End -->

			</div>
		</div>


<div style="text-align:center;margin:10px;padding:10px;">
	<button style="text-align:center; margin:0px auto;" onClick="javascript:location.reload(true)"> Generate New Quote </button>
</div>

<div style="text-align:center">
<script type="text/javascript"><!--
google_ad_client = "ca-pub-7937169317942721";
/* Boticus */
google_ad_slot = "8759166980";
google_ad_width = 468;
google_ad_height = 60;
//-->
</script>
<script type="text/javascript"
src="//pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
</div>

		<!-- #### Social Section -->



		<!-- #### Social Section - End -->

	</body>
</html>
<?php }} ?>