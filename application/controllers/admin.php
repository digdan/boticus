<?php
include_once("application/libraries/util.php");
class admin extends CI_Controller {

	function admin() {

		parent::__construct();
		$this->load->library(array('tank_auth','smarty','boticus'));
		$this->load->helper(array('form', 'url'));
		$this->ci =& get_instance();
		$this->ci->load->database();
	}

	function view() {
		$data["pre_content"] = $_POST["content"];
		$data["content"] = $this->boticus->decode($_POST["content"]);
		$this->smarty->view( 'admin_preview.tpl' , $data );
	}

	function send_tweet() {
		$this->load->library('tweet');
		$this->tweet->authenticate();
		$this->tweet->setStatus(strip_tags($_POST["tweet_content"]));
		$this->smarty->view( 'admin_tweet.tpl' , array());
	}

	function send_image($seed=NULL,$forced=NULL) {
		$from_name = "Boticus";
		$from_mail = "info@boticus.com";
		$to_name = "Facebook";
		$mailto = "barren317inject@m.facebook.com";
		//$mailto = "dan@danmorgan.net";
		$content = $this->boticus->content($forced,$seed);
		$message = $content["content"]. " ~ Boticus";
		$subject = "";
		if ( ! is_null($forced) ) {
			$file = "http://www.boticus.com/blank/{$seed}/{$forced}";
		} else {
			$file = "http://www.boticus.com/blank/{$seed}";
		}
		$content = file_get_contents($file);
		$file_size = sizeof($content);
		$content = chunk_split(base64_encode($content));
		$uid = md5(uniqid(time()));
		//$name = basename($file);
		$name = $seed.".jpg";
		$header = "From: ".$from_name." <".$from_mail.">\r\n";
		$header .= "MIME-Version: 1.0\r\n";
		$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
		$header .= "This is a multi-part message in MIME format.\r\n";
		$header .= "--".$uid."\r\n";
		$header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
		$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
		$header .= $message."\r\n\r\n";
		$header .= "--".$uid."\r\n";
		$header .= "Content-Type: application/octet-stream; name=\"".$seed.".jpg\"\r\n"; // use different content types here
		$header .= "Content-Transfer-Encoding: base64\r\n";
		$header .= "Content-Disposition: attachment; filename=\"".$seed.".jpg\"\r\n\r\n";
		$header .= $content."\r\n\r\n";
		$header .= "--".$uid."--";
		if (mail($mailto, $subject, $message, $header)) {
		    echo "<H1>PHOTO SENT TO FACEBOOK!</H1>"; // or use booleans here
		} else {
		    echo "<H1>ERROR! NOT SUBMITED!</H1>";
		}
	}

	function rand_img() {
		$data = array();
		$seed = rand(0,1000000);
		$data["seed"] = $seed;
		$this->smarty->view( 'admin_rand_img.tpl', $data );
	}

	function index($load_id=NULL) {
		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth');
		}

		$data = array();

		if ($this->input->post('title')) {
			$id = $this->input->post('id');
			if (!$id) { //New Pattern
				$tn = time();
				$this->ci->db->query($this->ci->db->insert_string("patterns",
				array(
					"name"=>$this->input->post('title'),
					"pattern"=>$this->input->post('content'),
					"created"=>$tn,
					"creator"=>$this->tank_auth->get_user_id()
					)
				));

				$id = $this->ci->db->insert_id();
			} else { //Update Pattern
				$dat = array(
					"name" => $this->input->post('title'),
					"pattern" => $this->input->post('content'),
				);
				$this->ci->db->query($this->ci->db->update_string("patterns",$dat,"id = {$id}"));
			}
			redirect("/admin/{$id}");
		} elseif ( ! is_null($load_id)) {
			$gquery = "SELECT * from patterns where id = '{$load_id}' limit 1";
			$query = $this->ci->db->query($gquery);
			$data["switch_accordian"] = ", active:1";
			$data["form"] = $query->row_array();
			$data["form"]["used_readable"] = util::readable_number($data["form"]["used"]);
		} else {
			$data["form"]["used_readable"] = "zilcho";
		}

		//Load patterns
		$pred = $this->db->query('SELECT * FROM patterns order by id desc ');
		foreach($pred->result_array() as $k=>$v) {
			$data["patterns"][] = $v;

		}

		$seed = rand(0,1000000);
		$data["seed"] = $seed;

		// Calling the convenience function view() that allows passing data
		$this->smarty->view( 'admin.tpl', $data );


	}
}
