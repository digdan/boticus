<?php

class fourofour extends CI_Controller {

	function fourofour() {
		parent::__construct();
		//$this->load->library('form_validation');
		$this->load->library('smarty');
	}

	function index() {
		$data = array();
		$this->smarty->view( '404.tpl', $data );

	}
}
