<?php

class front_page extends CI_Controller {

	function __construct() {
		parent::__construct();
		//$this->load->library('form_validation');
		$this->load->library(array('smarty','boticus'));
	}

	function blank($seed=NULL,$forced=NULL) {
		$this->boticus->image($forced,$seed);
		die();
	}

	function index() {
		if (@$_GET["forced"]) {
			$forced = $_GET["forced"];
		} else {
			$forced = NULL;
		}
		$data["meta"]["title"] = "Boticus";
		$content = $this->boticus->content($forced);
/*
		$url = "http://vozme.com/text2voice.php?lang=en&text=".urlencode($content["content"]);
		$fs = file($url);
		$mp3_url = "";
		foreach($fs as $k=>$v) {
			if (strstr($v,"<param ")) {
				$parts = explode("\"",$v);
				$mp3_url = str_replace("&amp;autoplay=1","",str_replace("dewplayer-multi.swf?mp3=","http://vozme.com/",$parts[3]));

			}
		}
		$content["mp3_url"] = $mp3_url;
*/
		$data["boticus"]["content"] = $this->smarty->view( 'front_content.tpl', $content , true);
		$data["boticus"]["random"] = crc32(microtime());
		$this->smarty->view( 'front_page.tpl', $data );

	}
}
