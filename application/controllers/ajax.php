<?
	class Ajax extends CI_Controller {

		function __construct() {
			parent::__construct();
			$this->load->library(array('boticus','smarty','tank_auth'));
			$this->ci =& get_instance();
			$this->ci->load->database();
		}

		function word($word) {
			$dat = $this->boticus->w($word);
			if (is_null($dat)) {
				echo "<I>None</I>";die();
			}
			$out = "<ul>";
			foreach($dat as $head=>$hd) {
				$out .= "<li>{$head}<ul>";
				foreach($hd as $owords) {
					foreach($owords as $oword) {
						$out .= "<li>{$oword}</li>";
					}
				}
				$out .= "</ul></li>";
			}
			$out .= "</ul>";
			echo $out; die();
		}

/**
* supply content for front page
*
*/
		function content() {
			echo $this->smarty->view( 'front_content.tpl', $this->boticus->content(), true);
			die();
		}

		function pattern_delete($pattern_id) {
			if ($this->tank_auth->is_logged_in()) {
				$this->ci->db->query("DELETE from patterns where id = {$pattern_id}");
				die();
			}
		}

	}
?>